import Vue from 'vue'
import {
  Vuetify,
  VApp,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VToolbar,
  VTextField,
  VTabs,
  VTooltip,
  VCard,
  VBadge,
  VChip,
  VDialog,
  VProgressLinear,
  VSnackbar,
  VDivider,
  transitions
} from 'vuetify'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VBadge,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VTooltip,
    VSnackbar,
    VIcon,
    VGrid,
    VToolbar,
    VDialog,
    VProgressLinear,
    VCard,
    VTextField,
    VTabs,
    VChip,
    VDivider,
    transitions
  },
  theme: {
    secondary: "#FF6F00",
    primary: "#FFA000",
    accent: "#5D4037",
    error: "#D32F2F",
    warning: "#FFEA00",
    info: "#039BE5",
    success: "#388E3C"
  },
})
